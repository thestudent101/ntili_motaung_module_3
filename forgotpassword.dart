import 'package:flutter/material.dart';

class Forgot extends StatelessWidget {
  const Forgot({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          elevation: .1,
          title: const Text("Forgot Password"),
          backgroundColor: const Color.fromRGBO(49, 87, 110, 1.0),
        ),
        body: SafeArea(
          child: ListView(
            padding: const EdgeInsets.symmetric(horizontal: 18.0),
            children: <Widget>[
              Column(
                // ignore: prefer_const_constructors
                children: [
                  const SizedBox(height: 15),
                  Image.asset('assets/images/aid3-.png'),
                  // ignore: prefer_const_constructors
                  SizedBox(height: 10),
                  const Text('Lets get you back into your account!',
                      style: TextStyle(
                          fontSize: 20,
                          color: Color.fromARGB(255, 56, 111, 162))),
                ],
              ),
              // ignore: prefer_const_constructors
              SizedBox(height: 40),
              const TextField(
                  decoration: InputDecoration(
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(
                        Radius.circular(10.0),
                      )),
                      labelText: "Email",
                      labelStyle:
                          TextStyle(fontSize: 20, color: Colors.orangeAccent),
                      filled: true)),

              const SizedBox(height: 40),
              Column(children: [
                ButtonTheme(
                    height: 40,
                    disabledColor: Colors.orangeAccent,
                    child: ElevatedButton(
                      onPressed: () {},
                      child: const Text('Reset Password',
                          style: TextStyle(
                            fontSize: 15,
                          )),
                    )),
              ])
            ],
          ),
        ));
  }
}
