// ignore_for_file: file_names

import 'package:flutter/material.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'login.dart';

class Onbordingscreen extends StatefulWidget {
  const Onbordingscreen({Key? key}) : super(key: key);

  @override
  State<Onbordingscreen> createState() => _OnbordingscreenState();
}

class _OnbordingscreenState extends State<Onbordingscreen> {
  // controller keeping track what page we are on.
  final PageController _controller = PageController();
  // Keep Track if we are on the last page or not
  // ignore: non_constant_identifier_names
  bool OnLastpage = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          elevation: .1,
          title: const Text("Intro Screens"),
          backgroundColor: const Color.fromRGBO(49, 87, 110, 1.0),
        ),
        body: Stack(
          children: [
            PageView(
              controller: _controller,
              onPageChanged: (index) {
                setState(() {
                  OnLastpage = (index == 1);
                });
              },
              children: [
                Container(
                  child: (Image.asset('assets/images/t1rem.png',
                      height: 200, width: 200)),
                  color: (Colors.white),
                ),
                Container(
                  child: (Image.asset('assets/images/t2rem.png',
                      height: 200, width: 200)),
                  color: (Colors.white),
                ),
                /*Container(
                  color: (Colors.greenAccent),
                ),*/
              ],
            ),
            Container(
              alignment: const Alignment(0, 0.75),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  //skip
                  GestureDetector(
                    onTap: () {
                      _controller.jumpToPage(1);
                    },
                    child: const Text('Skip'),
                  ),
                  //

                  //dot container
                  SmoothPageIndicator(controller: _controller, count: 2),

                  //Next or Done
                  OnLastpage
                      ? GestureDetector(
                          onTap: () {
                            Navigator.push(context,
                                MaterialPageRoute(builder: (context) {
                              return LoginPage(
                                showsignup: () {},
                              );
                            }));
                          },
                          child: const Text('Done'),
                        )
                      : GestureDetector(
                          onTap: () {
                            _controller.nextPage(
                              duration: const Duration(milliseconds: 500),
                              curve: Curves.easeIn,
                            );
                          },
                          child: const Text('Next'),
                        ),
                ],
              ),
            ),
          ],
        ));
  }
}
