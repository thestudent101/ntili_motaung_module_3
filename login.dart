import 'package:flutter/material.dart';

import 'Dashboard.dart';
import 'forgotpassword.dart';
import 'signup.dart';

class LoginPage extends StatefulWidget {
  final VoidCallback showsignup;
  const LoginPage({Key? key, required this.showsignup}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    //var column = Column(
    // ignore: prefer_const_literals_to_create_immutables
    //children: <widget>[],

    return Scaffold(
      appBar: AppBar(
        elevation: .1,
        title: const Text("Log in Screen"),
        backgroundColor: const Color.fromRGBO(49, 87, 110, 1.0),
      ),
      body: SafeArea(
        child: ListView(
          padding: const EdgeInsets.symmetric(horizontal: 18.0),
          children: <Widget>[
            Column(
              // ignore: prefer_const_constructors
              children: [
                const SizedBox(height: 15),
                Image.asset('assets/images/aid3-.png'),
                // ignore: prefer_const_constructors
                SizedBox(height: 10),
                const Text('Hello Again!',
                    style: TextStyle(
                        fontSize: 20,
                        color: Color.fromARGB(255, 56, 111, 162))),
              ],
            ),
            // ignore: prefer_const_constructors
            SizedBox(height: 40),
            const TextField(
              decoration: InputDecoration(
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(
                    Radius.circular(10.0),
                  )),
                  labelText: "Email",
                  labelStyle:
                      TextStyle(fontSize: 20, color: Colors.orangeAccent),
                  filled: true),
            ),
            const SizedBox(height: 20),
            const TextField(
              decoration: InputDecoration(
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(
                    Radius.circular(10.0),
                  )),
                  labelText: "Password",
                  labelStyle:
                      TextStyle(fontSize: 20, color: Colors.orangeAccent),
                  filled: true),
              obscureText: true,
            ),
            const SizedBox(
              height: 30,
            ),
            Column(
              children: [
                ButtonTheme(
                    height: 20,
                    disabledColor: Colors.orangeAccent,
                    child: ElevatedButton(
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => const Dashboard()));
                      },
                      child: const Text('Login'),
                    )),
                Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      TextButton(
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => const Forgot()));
                        },
                        child: const Text('Forget Password?'),
                      ),
                      TextButton(
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => const SignUpWidget()));
                        },
                        child: const Text('Sign Up?'),
                      )
                    ])
              ],
            ),
          ],
        ),
      ),
    );
  }
}
